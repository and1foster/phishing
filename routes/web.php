<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('nexus');
});
// Route::get('/test', function () {
//     return view('test');
// });
Route::resource('usuario', 'UsuarioController')->only(['store']);

Route::group([
    'name' => 'admin.',
    'prefix' => 'admin',
    'middleware' => 'auth'
], function () {
Route::get('/', 'HomeController@index')->name('home');
Route::resource('usuario', 'UsuarioController')->only(['index']);
Route::resource('movies', 'MovieController');
});
