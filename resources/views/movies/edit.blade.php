@extends('layouts.app')
@section('content')
<div class="content">
    <div class="container">
        <div class="row ">

            <div class="col-md-8">
                <form method="POST" action={{route('movies.update',['id' => $movie->id])}}>
                    @csrf
                    @method('PUT')
                    <div class="card">
                        {{-- card header--}}
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Información de película</h4>
                            <p class="card-category">Modificar</p>
                        </div>
                        {{-- card body--}}
                        <div class="card-body">
                            {{-- title --}}
                            <div class="form-group row">
                                <label for="title" class="col-4 col-form-label">Título</label>
                                <div class="col-8">
                                    <input id="title" value="{{$movie->title}}" name="title" type="text"
                                        class="form-control">
                                </div>
                            </div>
                            {{-- genre --}}
                            <div class="form-group row">
                                <label for="genre" class="col-4 col-form-label">Género</label>
                                <div class="col-8">
                                    <input id="genre" value="{{$movie->genre}}" name="genre" type="text"
                                        class="form-control">
                                </div>
                            </div>
                            {{-- description --}}
                            <div class="form-group row">
                                <label for="description" class="col-4 col-form-label">Descripción</label>
                                <div class="col-8">
                                    <input id="description" value="{{$movie->description}}" name="description" type="text"
                                        class="form-control">
                                </div>
                            </div>
                            {{-- year --}}
                            <div class="form-group row">
                                <label for="year" class="col-4 col-form-label">Año</label>
                                <div class="col-8">
                                    <input id="year" value="{{$movie->year}}" name="year" type="text"
                                        class="form-control">
                                </div>
                            </div>

                            {{-- buttons--}}
                            <div class="form-group row">
                                <div class="col-xs-offset-4 col-xs-8">
                                    <button name="submit" type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                                <a class="btn btn-light" href="{{ route('movies.index') }}" role="button">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
