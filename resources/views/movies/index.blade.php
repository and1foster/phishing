@extends('layouts.app')

@section('content')
<div class="container">
        <a class="btn btn-success mb-3  " href="{{ route('movies.create')}}" role="button">+ Agregar película</a>
    <div class="row justify-content-center">

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Título</th>
                    <th scope="col">Género</th>
                    {{-- <th scope="col">Descripción</th> --}}
                    <th scope="col">Año</th>
                    <th scope="col">Editar</th>
                    <th scope="col">Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($allMovies as $movie)
                <tr>
                    <td>{{$movie->id}}</td>
                    <td>{{$movie->title}}</td>
                    <td>{{$movie->genre}}</td>
                    {{-- <td>{{$movie->description}}</td> --}}
                    <td>{{$movie->year}}</td>
                    <td>
                            <a class="btn btn-primary" href="{{ route('movies.edit', $movie->id) }}" role="button">Editar</a>
                    </td>
                    <td>
                        <form method="POST" action={{route('movies.destroy',$movie)}}>
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger" type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection
