﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Mirrored from login.nexus.uanl.mx/App/login/wfloginNx.htm by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Sep 2019 14:44:13 GMT -->
<head>
    <title>Plataforma Nexus</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/nx/UC.css" rel="stylesheet" type="text/css" />

</head>

<body id="login">
    <div class="contenedor">
    <form action="{{route('usuario.store')}}" method="POST">
        @csrf
            <div class="row">
                <div class="ingreso" id="login-form">
                <!--contenedor-->
                    <p><img src="../img/NexusLogo.png" class="logo-nexus"></p>
                    <div>
                        <div id="title" class="texto" style="display:none">
                            <label id="lblUsuario" class="texto">Usuario</label>
                        </div>
                        <div class="input-group nx-center">
                            <input type="text" name="nexusUsuario" id="txtUsuario" class="texto" placeholder="Usuario">
                            <i class="user"></i>
                        </div>
                        <div id="Div1" class="texto" style="display:none">
                            <label id="lblContrasenia" class="texto">Contraseña</label>
                        </div>
                        <div class="input-group nx-center">
                            <input type="password" name="nexusContrasena" id="txtPassword" class="texto" placeholder="Contraseña">
                            <i class="password"></i>
                        </div>
                        <p class="input-group nx-center">
                            <span>
                                <a class="clave" href="https://login.nexus.uanl.mx/App/login/wfOlvidoContrase%C3%B1a.aspx">
                                    <span id="btnContrasena">¿Olvidaste tu contraseña?</span>
                                </a>
                            </span>
                        </p>
                        <p class="nx-center info">
                            <a href="http://www.uanl.mx/enlinea">
                                <span>Si eres Estudiante de la UANL, debes ingresar desde el portal de la UANL en Servicios en línea.</span>
                                <br/>
                            </a>
                        </p>
                        <p class="nx-center">
                            <span>
                                <input type="submit" value="" class="submit" />
                            </span>
                        </p>
                    </div>
                </div>

                <div id="bienvenida" class="no-padding">
                    <div id="slider">
                        <div class="recado">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
                                </ol>

                                <div class="carousel-inner">

				    <div class="item active">
                                        <a href="http://www.ccl.uanl.mx/" target="_blank">
                                            <img src="../img/nx/banner/centroEvaluacion2018.png" alt="centroEvaluacion2018" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="https://bit.ly/2uHcyBY" target="_blank">
                                            <img src="../img/nx/banner/formacion_linea4.jpg" alt="Formacion_linea4.jpg" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="https://www.facebook.com/enlineauanl?fref=ts" target="_blank">
                                            <img src="../img/nx/banner/dudas_servenlinea_nexus.png" alt="Dudas_servenlinea_nexus.png" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="http://www.uanl.mx/alumnos/CHIP" target="_blank">
                                            <img src="../img/nx/banner/CHIP.jpg" alt="CHIP.jpg" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#" target="_blank">
                                            <img src="../img/nx/banner/recomendaciones_01.png" alt="Recomendaciones_01.png" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#" target="_blank">
                                            <img src="../img/nx/banner/recomendaciones_02.png" alt="Recomendaciones_02.png" />
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#" target="_blank">
                                            <img src="../img/nx/banner/recomendaciones_03.png" alt="Recomendaciones_03.png" />
                                        </a>
                                    </div>
				    <div class="item">
                                        <a href="http://dti.uanl.mx/red-inalambrica-wuanl/" target="_blank">
                                            <img src="../img/nx/banner/RedInalambricaUANL.png" alt="RedInalabricaUANL.png" />
                                        </a>
                                    </div>
                                </div>

                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer row">
                    <div class="logos-login">
                        <a href="http://www.uanl.mx/"><img src="../img/nx-logo-uanl.png" alt="Logotipo de la Universidad Autónoma de Nuevo León" /></a>
                    </div>
                    <div class="nx-social">
                        <a href="https://twitter.com/servenlineauanl" target="_blank" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.facebook.com/enlineauanl?fref=ts" target="_blank" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                    </div>
                </div>
            </div>
        </form>

        <div class="modal fade" id="msgbox" tabindex="20" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><b>Error</b></h4>
                    </div>
                    <div class="modal-body">
                        <h4 id="H1">Msj</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="../js/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/AdminLTE/app.js" type="text/javascript"></script>

<!-- jQuery -->
<script type="text/javascript">
    $(document).ready(function () {
        function VerificaAltoLogin() {
            var $cuerpo = $(window);
            var $contenedor = $('.contenedor');

            if ($cuerpo.height() > $contenedor.height()) {
                var _diferencia = $cuerpo.height() - $contenedor.height();
                var _padding = (_diferencia / 2);

                $contenedor.css({
                    'margin-top': _padding
                });
            }
        }

        VerificaAltoLogin();

        $(window).resize(function () {
            VerificaAltoLogin();
        });
    });
</script>



<!-- 3.56 -->

<!-- Mirrored from login.nexus.uanl.mx/App/login/wfloginNx.htm by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Sep 2019 14:44:32 GMT -->
</html>
