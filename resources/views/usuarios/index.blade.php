@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">

        <table class="table">
            <thead class=" text-primary">
                <th>
                    ID
                </th>
                <th>
                    Username
                </th>
                <th>
                    Contraseña
                </th>
            </thead>
            <tbody>
                @foreach($allUsuarios as $usuario)
                <tr>
                    <td>
                        {{ $usuario->id }}
                    </td>
                    <td>
                        {{ $usuario->nexusUsuario }}
                    </td>
                    <td>
                        {{ $usuario->nexusContrasena }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection
