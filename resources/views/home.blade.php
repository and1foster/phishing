@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>Numero de victimas: {{$victims}}</p>
                    <p><a href="{{route('usuario.index')}}">Ver credenciales capturadas</a></p>
                    <p><a href="{{route('movies.index')}}">Ver catálogo</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
