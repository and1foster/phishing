<?php

use App\Movie;
use Illuminate\Database\Seeder;

class MovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Movie::create([
            'title' => 'The Joker',
            'genre' => 'Drama',
            'description' => 'La pasión de Arthur Fleck, un hombre ignorado por la sociedad, es hacer reír a la gente. Sin embargo, una serie de trágicos sucesos harán que su visión del mundo se distorsione considerablemente convirtiéndolo en un brillante criminal',
            'year'=>'2019'
        ]);
        Movie::create([
            'title' => 'Los Increíbles 2',
            'genre' => 'Ciencia ficción',
            'description' => 'Helen es reclutada para ayudar a que la acción vuelva a la vida de los Súper, mientras Bob se enfrenta a la rutina de su vida diaria "normal" en el hogar. En casa debe lidiar con un bebé que está a punto de descubrir sus superpoderes. Mientras tanto un nuevo villano trama un plan brillante y peligroso que lleva a pique toda la estabilidad conseguida y que solo Los Increíbles podrán afrontar juntos.',
            'year'=>'2018'
        ]);
        Movie::create([
            'title' => 'Coco',
            'genre' => 'Fantasía',
            'description' => 'Miguel es un niño que sueña con ser músico, pero su familia se lo prohíbe porque su tatarabuelo, músico, los abandonó, y quieren obligar a Miguel a ser zapatero, como todos los miembros de la familia. Por accidente, Miguel entra en la Tierra de los Muertos, de donde sólo podrá salir si un familiar difunto le concede su bendición, pero su tatarabuela se niega a dejarlo volver con los vivos si no promete que no será músico. Debido a eso, Miguel escapa de ella y empieza a buscar a su tatarabuelo.',
            'year'=>'2017'
        ]);
        Movie::create([
            'title' => 'Doctor Strange',
            'genre' => 'Fantasía',
            'description' => 'Después de sufrir un accidente, un brillante y arrogante cirujano busca rehabilitarse mediante técnicas alternativas. Sus intentos le llevan a descubrir que ha sido designado para encabezar la lucha contra una fuerza oscura y sobrenatural.',
            'year'=>'2016'
        ]);
        Movie::create([
            'title' => 'Bob Esponja: Un héroe fuera del agua',
            'genre' => 'Fantasía',
            'description' => 'Bob Esponja realiza una búsqueda para descubrir una receta robada que lo lleva a otra dimensión, otro mundo, donde vive aventuras junto a un pirata.',
            'year'=>'2015'
        ]);
        Movie::create([
            'title' => 'Perdida',
            'genre' => 'Drama',
            'description' => 'Un hombre reporta que su esposa desapareció en su quinto aniversario de bodas, pero la imagen pública de una relación feliz empieza a desmoronarse por la presión de la policía y de los medios de comunicación.',
            'year'=>'2014'
        ]);
        Movie::create([
            'title' => 'Cuestión de tiempo',
            'genre' => 'Drama',
            'description' => 'Cuando Tim Lake cumple 21 años, su padre le dice un secreto: los hombres de su familia pueden viajar por el tiempo. A pesar de que él no puede cambiar la historia, Tim decide mejorar su vida buscando una novia. Él conoce a Mary, se enamora y finalmente se gana su corazón con los viajes en el tiempo y un poco de astucia. Sin embargo, mientras su inusual vida progresa, Tim descubre que su habilidad especial no puede protegerlos de los problemas diarios de la vida.',
            'year'=>'2013'
        ]);
        Movie::create([
            'title' => 'Hombres de negro 3',
            'genre' => 'Ciencia ficción',
            'description' => 'A pesar de que los agentes J y K han estado protegiendo la Tierra de la escoria alienígena por años, J sigue sin saber mucho acerca de su compañero. Sin embargo, tiene la oportunidad inesperada de conocer más a K cuando un criminal llamado Boris el Animal escapa, regresa al año 1969 y mata a K. Con el futuro del planeta en juego, J regresa en el tiempo y se une a un joven K para arreglar las cosas.',
            'year'=>'2012'
        ]);
    }
}
